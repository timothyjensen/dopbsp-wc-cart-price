# Pinpoint Booking System — WooCommerce Cart Price
This plugin patches a bug in the Pinpoint Booking System plugin v2.7.4 where WooCommerce cart and checkout prices do not reflect the booking price. The plugin author may release a future fix at which time this plugin can be removed.
