<?php
/**
 * Pinpoint Booking System — WooCommerce Cart Price
 *
 * @package     TimJensen\DOPBSPWCCartPrice
 * @author      Tim Jensen <tim@timjensen.us>
 * @license     GPL-2.0-or-later
 *
 * @wordpress-plugin
 * Plugin Name: Pinpoint Booking System — WooCommerce Cart Price
 * Plugin URI:  https://bitbucket.org/timothyjensen/dopbsp-wc-cart-price
 * Description: Modifies the WooCommerce cart/checkout price to match the booking price.
 * Version:     1.0.0
 * Author:      Tim Jensen
 * Author URI:  https://www.timjensen.us
 * Text Domain: dopbsp-wc-cart-price
 * License:     GPL-2.0-or-later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 */

namespace TimJensen\DOPBSPWCCartPrice;

if ( ! defined( 'ABSPATH' ) ) {
	die;
}


add_action( 'woocommerce_before_calculate_totals', __NAMESPACE__ . '\\update_cart_price' );
/**
 * Updates the price of reservation items found in the cart.
 *
 * @param \WC_Cart $cart_object WooCommerce Cart object.
 */
function update_cart_price( $cart_object ) {
	global $wpdb, $DOPBSPWooCommerce;

	// Do not run in WP Admin.
	if ( is_admin() && ! defined( 'DOING_AJAX' ) ) {
		return;
	}

	// Ensure Pinpoint Booking System PRO is active before proceeding.
	if ( ! class_exists( 'DOPBSPWooCommerceCart' ) ) {
		return;
	}

	/**
	 * The following code is a slight modification of DOPBSPWooCommerceCart->update. That method attempts to update the
	 * cart price but is unsuccessful. The plugin author may release a future fix, which will remove the need for this plugin.
	 */
	$cart = $cart_object->get_cart();
	foreach ( $cart as $cart_item_key => $cart_item ) {
		if ( isset( $cart_item['dopbsp_token'] ) ) {
			$reservations_data = $wpdb->get_results( $wpdb->prepare( 'SELECT * FROM ' . $DOPBSPWooCommerce->tables->woocommerce . ' WHERE cart_item_key="%s" AND token="%s" AND product_id=%d',
				$cart_item_key, $cart_item['dopbsp_token'], $cart_item['product_id'] ) );

			$new_cart_item_price = 0;
			foreach ( $reservations_data as $reservation_data ) {
				$reservation         = json_decode( $reservation_data->data );
				$new_cart_item_price += $reservation->price_total;
			}

			$cart_item['data']->set_price( $new_cart_item_price );
		}
	}
}

